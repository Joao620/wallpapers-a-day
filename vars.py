import random

class randoCores(list):
    def __init__(self, cores: list[int]) -> None:
        self.cores = list(map(lambda x: x | 0xff000000, cores))
        super().__init__(self.cores)
        self.quant = len(cores)
        pass
    
    def get(self, peso: int) -> int:
        """
        Peso é a quantidade de itens que serão incluidas na sortagem
        Numero positivos são para incluir do começo da lista, negativos do final
        
        A porcentagem dos itens que sairam serão distribuidas de maneira linear para o resto
        """
        if peso == 0:
            raise ValueError("zero não ⋋_⋌")
        
        distribuidos = abs(peso) * 100
        if peso < 0:
            lista_pa = self.cores[:peso]
        else:
            # precisa inverter para a prioridade ser as cores mais claras (que ficam no final)
            lista_pa = self.cores[peso::-1]
        len_pa = len(lista_pa)
        
        magic = lambda x: distribuidos - (distribuidos/len_pa * x)
        pesos_mesmo = [magic(i) for i in range(len_pa)]
        return random.choices(lista_pa, weights=pesos_mesmo, k=1)[0]
    
    def random(self) -> int:
        return random.choice(self)
    
    def randoms(self, n: int) -> list[int]:
        return random.choices(self, k=n)
    
#other colors here: https://tinted-theming.github.io/base16-gallery/
#Atlas color pallet https://github.com/tinted-theming/base16-vim
background = randoCores(
    [0x002635, 0x00384d, 0x517F8D, 0x6C8B91, 0x869696, 0xa1a19a, 0xe6e6dc, 0xfafaf8]
)

accent = randoCores(
    [0xff5a67, 0xf08e48, 0xffcc1b, 0x7fc06e, 0x5dd7b9, 0x14747e, 0x9a70a4, 0xc43060]
)

monitor = (1366, 768)
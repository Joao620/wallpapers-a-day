from datetime import date
import os
import random
import importlib

sketchs = filter(lambda file: file.endswith('.py'), os.listdir('sketchs'))
random_sketch = random.choice(list(sketchs))[:-3] # tirar o '.py' do nome do arquivo

dia_e_mes = int(date.today().strftime("%d%m"))
random.seed(dia_e_mes)

importlib.import_module(f'sketchs.{random_sketch}')

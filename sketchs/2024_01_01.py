from itertools import product, combinations
import numpy as np
import py5
from random import choice

import vars

num_species = 4
num_particles = 500
dt = 0.1
largura, altura = vars.monitor


def setup():
    global positions, velocities, species, species_masks, radius, pairs    
    py5.size(largura, altura)
    py5.color_mode(py5.HSB)
    
    positions = np.random.rand(num_particles, 2)
    angles = 2.0 * np.pi * np.random.random(num_particles)
    speed = 0.01
    velocities = np.stack((speed * np.cos(angles),
                           speed * np.sin(angles)), axis=1)
    species = np.random.randint(0, num_species, size=num_particles)
    species_masks = [species == i for i in range(num_species)] 
    pairs = np.array(list(combinations(range(num_particles), 2)))  # tem jeito melhor se for repetir
    py5.background(vars.background.get(-5))

linhas_sombras = vars.background.get(3)
def draw():

    global positions
    difference_vectors = positions[pairs[:, 1]] - positions[pairs[:, 0]]
    #squared_distances = np.sum(difference_vectors**2, axis=1)
    squared_distances = np.einsum('ij,ij->i', difference_vectors, difference_vectors)
    # assuming a  square canvas... for drawing
    scaled_positions = positions * py5.width
    # find particles that are close to each other
    selected_pairs = pairs[squared_distances < 1 / 800]
    if selected_pairs.any():
        py5.stroke(linhas_sombras)
        py5.stroke_weight(0.01)
        py5.lines(np.hstack((scaled_positions[selected_pairs[:, 0]],
                             scaled_positions[selected_pairs[:, 1]])))
    # draw particles
    for i, mask in enumerate(species_masks):
        cor = vars.accent[i % len(vars.accent)]
        py5.stroke(cor)
        py5.stroke_weight(1)
        py5.points(scaled_positions[mask])

    positions += dt * velocities # update positions
    positions %= 1 # wrap positions
    py5.window_title(f'{py5.get_frame_rate():.1f}')

    if py5.frame_count == 200:
        py5.no_loop()
        
py5.run_sketch()
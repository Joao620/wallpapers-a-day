
from itertools import product, combinations
import numpy as np
import py5
import random
import vars

num_species = 4
num_particles = 500
dt = 0.1

def setup():
    global positions, velocities, species, species_masks, radius, pairs, ps, height_width_ratio
    py5.size(*vars.monitor, py5.P2D)
    #py5.color_mode(py5.HSB)
    
    height_width_ratio = py5.height / py5.width
    positions = np.random.uniform(low=[0, 0], high=[1, height_width_ratio], size=(num_particles, 2))
    angles = 2.0 * np.pi * np.random.random(num_particles)
    
    speed = 0.01
    velocities = np.stack((speed * np.cos(angles),
                           speed * np.sin(angles)), axis=1)
    species = np.random.randint(0, num_species, size=num_particles)
    species_masks = [species == i for i in range(num_species)] 
    pairs = np.array(list(combinations(range(num_particles), 2)))  # tem jeito melhor se for repetir
    py5.background(vars.background.get(-5))

possivel_cores = [*vars.accent, *vars.background[5:]]
(x1, x2, y1, y2) = tuple(map(
    lambda c: py5.color(c),
    random.sample(possivel_cores, k=4)
))

def lerp_that_boi(x, y):
    color_y1 = py5.lerp_color(x1, x2, (x % 255) / 255)
    color_y2 = py5.lerp_color(y1, y2, (x % 255) / 255)

    # Interpolate along y
    final_color = py5.lerp_color(color_y1, color_y2, (y % 255) / 255)
    
    return final_color
    
def draw():
    global positions
    difference_vectors = positions[pairs[:, 1]] - positions[pairs[:, 0]]
    #squared_distances = np.sum(difference_vectors**2, axis=1)
    squared_distances = np.einsum('ij,ij->i', difference_vectors, difference_vectors)
    # assuming a  square canvas... for drawing
    scaled_positions = positions * py5.width
#     if selected_pairs.any():
#         py5.stroke(255)
#         py5.stroke_weight(0.01)
#         py5.lines(np.hstack((scaled_positions[selected_pairs[:, 0]],
#                              scaled_positions[selected_pairs[:, 1]])))
    # draw particles
    for i, mask in enumerate(species_masks):
        ps = py5.create_shape()
        py5.stroke(py5.color(i * 256 / num_species, 255, 255))
        ps.begin_shape(py5.POINTS)
        ps.stroke_weight(2)
        ps.vertices(scaled_positions[mask])
        ps.end_shape()
        #ps.set_strokes(py5.color(x % 256, y % 256, 128) for x, y in scaled_positions[mask])
        ps.set_strokes(lerp_that_boi(x, y) for x, y in scaled_positions[mask])
        py5.shape(ps, 0, 0)

    positions += dt * velocities # update positions
    #positions %= 1 # wrap positions
    positions[:, 0] %= 1  # wrap x component (first column of the array) by the width of the screen
    positions[:, 1] %= height_width_ratio  # wrap y component (second column of the array) by the height of the screen
    py5.window_title(f'{py5.get_frame_rate():.1f}')

    if py5.frame_count == 200:
        py5.exit_sketch()

py5.run_sketch()